//
//  BookDetail.swift
//  BookAss
//
//  Created by Narith on 3/1/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import Foundation

class BookDetail {
    
    var bookId: String!
    var title: String!
    var price: Double!
    var author: String!
    var imgUrl: String!
    
    init(bookId: String,title:String, price: Double, author: String, imgUrl: String) {
        self.bookId = bookId
        self.imgUrl = imgUrl
        self.title = title
        self.author = author
        self.price = price
    }
}
