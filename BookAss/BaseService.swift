//
//  BaseService.swift
//  BookAss
//
//  Created by Narith on 3/1/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import Foundation
import Alamofire

class BaseService{
    
    var booklist: [Book] = []
    var book: Book!
    var bookDetail: BookDetail!
//    var completeHandle: (() -> Void)?
    let url = "http://assignment.gae.golgek.mobi/api/v1/items"
    
    func getBook(param: Dictionary<String, Any>){
        Alamofire.request(url, method: .get, parameters:param, encoding: URLEncoding.default, headers: [:]).responseJSON{response in
            switch response.result{
            case .success:
                let data = response.result.value! as! NSArray
                    for i in 0..<data.count{
                        let boookObj = data[i] as! NSDictionary
                        let bookid = boookObj["id"] as! String
                        let booktitle = boookObj["title"] as! String
                        let booklink = boookObj["link"] as! String
                        let bookprice = boookObj["price"] as! Double
                        self.book = Book(bookId: bookid , title: booktitle, price: bookprice, link: booklink)
                        self.booklist.append(self.book)
                    }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "finishRequestBookData"), object: nil, userInfo: nil)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getBookDetail(id: String){
        let urls = url+"/"+"\(id)" as String
        Alamofire.request(urls, method: .get, parameters: nil, encoding: URLEncoding.default, headers: [:]).responseJSON{response in
            switch response.result{
            case .success:
                let bookdetail = response.result.value! as! NSDictionary
                let bookid = bookdetail["id"] as! String
                let title = bookdetail["title"] as! String
                let image = bookdetail["image"] as! String
                let price = bookdetail["price"] as! Double
                let author = bookdetail["author"] as! String
                self.bookDetail = BookDetail(bookId: bookid, title: title, price: price, author: author, imgUrl: image)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "finishRequestBookData"), object: nil, userInfo: nil)
//                self.completeHandle?()
            case .failure(let error):
                print(error)
            }
        }

    }
}
