//
//  Book.swift
//  BookAss
//
//  Created by Narith on 3/1/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import Foundation

class Book {
    
    var bookId: String!
    var title: String!
    var price: Double!
    var link: String!
    
    init(bookId: String, title: String, price: Double ,link: String ) {
        self.bookId = bookId
        self.title = title
        self.price = price
        self.link = link
        
    }
}
