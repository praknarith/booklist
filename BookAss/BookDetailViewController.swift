//
//  BookDetailViewController.swift
//  BookAss
//
//  Created by BENITEN on 3/9/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit
import SDWebImage

class BookDetailViewController: UIViewController {

    var bookidLabel: UILabel!
    var imageview: UIImageView!
    var titleLabel: UILabel!
    var authorLabel: UILabel!
    var priceLabel: UILabel!
    
    var titleLbl: UILabel!
    var priceLbl: UILabel!
    var authorLbl: UILabel!
    var bookidLbl: UILabel!
    var bookId: String = ""
    
    let requestBookApi = BaseService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestBookApi.getBookDetail(id: bookId)
        NotificationCenter.default.addObserver(self, selector: #selector(finishRequestBookData), name: NSNotification.Name(rawValue: "finishRequestBookData"), object: nil)
        initComponents()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initComponents(){

      
        self.imageview = UIImageView(frame: CGRect(x: 0, y: 10, width: self.view.frame.width, height: 200))
        view.addSubview(imageview)
        
        self.titleLabel = UILabel(frame: CGRect(x: 10, y: self.imageview.frame.height + 12, width: 100, height: 30))
        self.titleLabel.text = "Title      :"
        self.view.addSubview(self.titleLabel)
        self.titleLbl = UILabel(frame: CGRect(x: self.titleLabel.frame.width + 20, y: self.imageview.frame.height + 12, width: view.frame.width - self.titleLabel.frame.width + 20, height: 30))
        self.titleLbl.text = "text"
        self.view.addSubview(titleLbl)
        
        self.authorLabel = UILabel(frame: CGRect(x: 10, y: self.titleLabel.frame.origin.y + 40, width: 100, height: 30))
        self.authorLabel.text = "Author  :"
        self.view.addSubview(self.authorLabel)
        self.authorLbl = UILabel(frame: CGRect(x: self.authorLabel.frame.width + 20, y: self.titleLbl.frame.origin.y + 40, width: view.frame.width - self.authorLabel.frame.width + 20, height: 30))
        self.authorLbl.text = "text"
        self.view.addSubview(authorLbl)
        
        self.priceLabel = UILabel(frame: CGRect(x: 10, y: self.authorLbl.frame.origin.y + 40, width:100, height: 30))
        self.priceLabel.text = "Price     :"
        self.view.addSubview(self.priceLabel)
        self.priceLbl = UILabel(frame: CGRect(x: self.priceLabel.frame.width + 20, y: self.authorLbl.frame.origin.y + 40, width: view.frame.width - self.authorLbl.frame.width + 20, height: 30))
        self.priceLbl.text = "text"
        self.view.addSubview(priceLbl)
    }
    
    func getBookDetail(){
        self.title = requestBookApi.bookDetail.title
        self.imageview.sd_setImage(with: URL(string: requestBookApi.bookDetail.imgUrl))
        self.titleLbl.text = requestBookApi.bookDetail.title
        self.authorLbl.text = requestBookApi.bookDetail.author
        self.priceLbl.text = String(format:"%f", requestBookApi.bookDetail.price)
    }
    
    func finishRequestBookData(){
        NotificationCenter.default.removeObserver(self)
        getBookDetail()
    }

    
}
