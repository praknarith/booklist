//
//  ViewController.swift
//  BookAss
//
//  Created by Narith on 3/1/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController {

    let requestBook = BaseService()
    
    var refreshController: UIRefreshControl!
    var refreshControllers: UIRefreshControl!
    var collectionView: UICollectionView!
    
    let cellId = "cell"
    let callback = "onGetItems"
    var offset: Int = 0
    var param: Dictionary<String, Any>!
    
    var dataArray = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        offset = 0
        param = ["offset": offset,"count":10]
        requestBook.getBook(param: param)
        NotificationCenter.default.addObserver(self, selector: #selector(finishRequestBookData), name: NSNotification.Name(rawValue: "finishRequestBookData"), object: nil)
        
        initCollectionView()
        initHeader()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func initCollectionView(){
        refreshController = UIRefreshControl()
        refreshController.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.sectionInset = UIEdgeInsetsMake(5,5, 5, 5 )
        
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: flowLayout)
        collectionView.backgroundColor = UIColor(red: 83/255, green: 59/255, blue: 77/255, alpha: 1.0)
        collectionView.alwaysBounceVertical = true;
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(BookCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.addSubview(refreshController)
        view.addSubview(collectionView)
        
    }

    func initHeader(){
        title = "Book List"
        navigationController?.navigationBar.barTintColor =  UIColor(red: 83/255, green: 59/255, blue: 77/255, alpha: 1.0)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = UIColor(red: 46/255, green: 161/255, blue: 129/255, alpha: 1.0)
        navigationController?.navigationBar.barStyle = .black
    }
    
    func refreshAction(){
        NotificationCenter.default.addObserver(self, selector: #selector(finishRequestBookData), name: NSNotification.Name(rawValue: "finishRequestBookData"), object: nil)
        requestBook.getBook(param: param)
        
    }
    
    func finishRequestBookData(){
        NotificationCenter.default.removeObserver(self)
        collectionView.reloadData()
        
        if refreshController.isRefreshing{
            refreshController.endRefreshing()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let endScrolling: CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
        if endScrolling >= scrollView.contentSize.height {
            if requestBook.booklist.count != 0{
                self.offset += 20
                param = ["offset": offset,"count":20]
                DispatchQueue.main.async(execute: {
                    self.requestBook.getBook(param: self.param)
                    NotificationCenter.default.addObserver(self, selector: #selector(self.finishRequestBookData), name: NSNotification.Name(rawValue: "finishRequestBookData"), object: nil)
                })
                
            }
        }
    }
    
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return requestBook.booklist.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! BookCollectionViewCell
        
        let book = requestBook.booklist[indexPath.row]
        cell.bookTitle.textColor = UIColor.white
        cell.bookTitle.text = book.title
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let channel = requestBook.booklist[indexPath.row]
        let detailView = BookDetailViewController()
        detailView.bookId = channel.bookId
        DispatchQueue.main.async(execute: {
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action:nil)
            self.navigationController?.pushViewController(detailView, animated: true)
        })
    }
    

}
