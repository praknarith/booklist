//
//  BookCollectionViewCell.swift
//  BookAss
//
//  Created by BENITEN on 3/9/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit

class BookCollectionViewCell: UICollectionViewCell{
    
    override init(frame: CGRect){
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        self.backgroundColor = UIColor(red: 46/255, green: 161/255, blue: 129/255, alpha: 1.0)
        self.addSubview(bookTitle)
    }
    
//    let bookImageView: UIImageView = {
//        let imageView = UIImageView()
//        imageView.contentMode = .scaleAspectFit
//        return imageView
//    }()
    
    let bookTitle: UILabel = {
        let title = UILabel()
        return title
    }()
    
    override func layoutSubviews() {
//        self.bookImageView.frame = CGRect(x: 5, y: 5, width: self.bounds.width - 10, height: self.bounds.height - 10)
        self.bookTitle.frame = CGRect(x: 5, y: 5, width: self.bounds.width - 5, height: self.bounds.height - 5)
    }
}
